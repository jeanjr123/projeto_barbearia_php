<?php
//dirname file vai para o diretorio que esta, e o parametro 2 leva ao pai do diretorio
    // que esta, ou seja para o diretorio raiz
    require_once(dirname(__FILE__, 2) . "/src/config/config.php");

    //index vai carregar as paginas dinamicamente
    //de acordo com a requisicao se monta o controller
    
    
    $uri = urldecode(
         parse_url($_SERVER['REQUEST_URI'] , PHP_URL_PATH)
    );
    
    
    if($uri === ''  || $uri === '/' || $uri === '/index.php'){
        //tenta carregar a home se tiver usuario de sessao
        $uri = '/home.php';
    }
    
    
    require_once(CONTROLLER_PATH . "/${uri}" );
    





