-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 24/03/2020 às 02:06
-- Versão do servidor: 10.4.11-MariaDB
-- Versão do PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `barbearia`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `agendamento`
--

CREATE TABLE `agendamento` (
  `idAgendamento` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `horaInicio` datetime DEFAULT NULL,
  `horaFim` datetime DEFAULT NULL,
  `idCabelereiro` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `idSalao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `agendamento`
--

INSERT INTO `agendamento` (`idAgendamento`, `data`, `horaInicio`, `horaFim`, `idCabelereiro`, `idCliente`, `idSalao`) VALUES
(2, '2020-02-01', '2020-02-01 09:00:00', '2020-02-01 10:15:00', 34, 33, 1),
(3, '2020-02-02', '2020-02-02 12:00:00', '2020-02-02 13:00:00', 34, 32, 1),
(60, '2020-03-22', '2020-03-22 16:00:00', '2020-03-22 17:15:00', 34, 32, 1),
(61, '2020-03-22', '2020-03-22 10:00:00', '2020-03-22 11:15:00', 34, 32, 1),
(62, '2020-03-22', '2020-03-22 12:00:00', '2020-03-22 12:35:00', 34, 32, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cidade`
--

CREATE TABLE `cidade` (
  `idCidade` int(11) NOT NULL,
  `nomeCidade` varchar(100) DEFAULT NULL,
  `idEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `cidade`
--

INSERT INTO `cidade` (`idCidade`, `nomeCidade`, `idEstado`) VALUES
(9, 'FRANCA', 15),
(11, 'Belo Horizonte', 17),
(22, 'adiamantina', 17);

-- --------------------------------------------------------

--
-- Estrutura para tabela `estado`
--

CREATE TABLE `estado` (
  `idEstado` int(11) NOT NULL,
  `nomeEstado` varchar(20) DEFAULT NULL,
  `sigla` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `estado`
--

INSERT INTO `estado` (`idEstado`, `nomeEstado`, `sigla`) VALUES
(15, 'SAO PAULO', 'SP'),
(17, 'MINAS GERAIS', 'MG'),
(20, 'ALAGOAS ', 'AL'),
(30, 'distrito federal', 'df'),
(31, 'amapa', 'am');

-- --------------------------------------------------------

--
-- Estrutura para tabela `salao`
--

CREATE TABLE `salao` (
  `idSalao` int(11) NOT NULL,
  `razaoSocial` varchar(100) DEFAULT NULL,
  `nomeFantasia` varchar(100) DEFAULT NULL,
  `cnpj` varchar(20) DEFAULT NULL,
  `inscricaoEstadual` varchar(20) DEFAULT NULL,
  `telefone1` varchar(20) DEFAULT NULL,
  `telefone2` varchar(20) DEFAULT NULL,
  `enderecoLogradouro` varchar(100) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `bairro` varchar(80) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `idCidade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `salao`
--

INSERT INTO `salao` (`idSalao`, `razaoSocial`, `nomeFantasia`, `cnpj`, `inscricaoEstadual`, `telefone1`, `telefone2`, `enderecoLogradouro`, `numero`, `bairro`, `cep`, `email`, `idCidade`) VALUES
(1, 'barbearia chico julio ltda', 'barbearia chico julio', '03.100.513/0001-65', '410.525.252.5', '(16) 37035-533', '(16) 3727-5566', 'avenida santa cruz', '5535', 'vila santa cruz', '14.400-000', 'barbearia@gmail.com', 9);

-- --------------------------------------------------------

--
-- Estrutura para tabela `servico`
--

CREATE TABLE `servico` (
  `idServico` int(11) NOT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `tempoEstimado` time DEFAULT NULL,
  `preco` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `servico`
--

INSERT INTO `servico` (`idServico`, `descricao`, `tempoEstimado`, `preco`) VALUES
(1, 'CORTE CABELO', '00:25:00', '25.00'),
(2, 'SOBRANCELHA', '00:10:00', '15.00'),
(47, 'CORTE BARBA', '00:40:00', '20.00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `servicosagendados`
--

CREATE TABLE `servicosagendados` (
  `idServicosAgendados` int(11) NOT NULL,
  `idAgendamento` int(11) NOT NULL,
  `idServico` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `servicosagendados`
--

INSERT INTO `servicosagendados` (`idServicosAgendados`, `idAgendamento`, `idServico`) VALUES
(1, 2, 1),
(2, 3, 2),
(3, 2, 2),
(4, 3, 47),
(5, 60, 1),
(6, 60, 2),
(7, 60, 47),
(8, 61, 1),
(9, 61, 2),
(10, 61, 47),
(11, 62, 1),
(12, 62, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipo`
--

CREATE TABLE `tipo` (
  `idTipo` int(11) NOT NULL,
  `descricaoTipo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `tipo`
--

INSERT INTO `tipo` (`idTipo`, `descricaoTipo`) VALUES
(1, 'CLIENTE'),
(2, 'CABELEIREIRO');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `login` varchar(20) NOT NULL,
  `nomeUsuario` varchar(100) NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `rg` varchar(20) DEFAULT NULL,
  `enderecoLogradouro` varchar(100) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `bairro` varchar(80) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `telefoneFixo` varchar(20) DEFAULT NULL,
  `dataNasc` varchar(10) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `senha` varchar(200) NOT NULL,
  `status` enum('ATIVO','INATIVO') NOT NULL,
  `idCidade` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `login`, `nomeUsuario`, `cpf`, `rg`, `enderecoLogradouro`, `numero`, `bairro`, `cep`, `celular`, `telefoneFixo`, `dataNasc`, `email`, `senha`, `status`, `idCidade`, `idTipo`) VALUES
(32, 'jean', 'jean junior silva de sousa', '41092909885', '489444210', 'rua ana lucia alves da silva cintra', '5535', 'sao geronimo', '14412402', '992843935', '1515151515', '06/01/1993', 'jeanjr.silvasousa@gmail.com', '$2y$10$/vC1UKrEJQUZLN2iM3U9re/4DQP74sXCOVXlYXe/j9zuv1/MHD4o.', 'ATIVO', 9, 1),
(33, 'kaique', 'kaique henrique silva de sousa', '40592522200', '4522211100', 'RUA EMILIO BERTONI', '2162', 'JD PETRAGLIA', '14409108', '992225588', '35352525', '22/05/1998', 'kaiquessh@hotmail.com', '123456', 'ATIVO', 9, 1),
(34, 'rodrigo', 'Rodrigo Pereira', '40511122200', '4855544411', 'avenida santa cruz', '5532', 'santa cruz', '14400000', '16995552222', '1635252222', '25/11/1983', 'rodrigobarbeiro@gmail.com', '123', 'ATIVO', 9, 2),
(50, 'nicole', 'nick freita', '410.929.098-85', '48.944.421-0', 'Rua Ana Lucia Alves da Silva Cintra', '5535', 'Residencial S&atilde;o Jer&ocirc;nimo', '14.412-402', '(16) 99345-4602', '(16) 9925-5555', '1993-09-10', 'nicole@gmail.com', '123', 'ATIVO', 9, 1);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `agendamento`
--
ALTER TABLE `agendamento`
  ADD PRIMARY KEY (`idAgendamento`),
  ADD KEY `fk_agendamento_usuario1_idx` (`idCabelereiro`),
  ADD KEY `fk_agendamento_usuario2_idx` (`idCliente`),
  ADD KEY `idSalao` (`idSalao`);

--
-- Índices de tabela `cidade`
--
ALTER TABLE `cidade`
  ADD PRIMARY KEY (`idCidade`),
  ADD KEY `fk_cidade_estado1_idx` (`idEstado`);

--
-- Índices de tabela `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idEstado`);

--
-- Índices de tabela `salao`
--
ALTER TABLE `salao`
  ADD PRIMARY KEY (`idSalao`),
  ADD KEY `fk_salao_cidade1_idx` (`idCidade`);

--
-- Índices de tabela `servico`
--
ALTER TABLE `servico`
  ADD PRIMARY KEY (`idServico`);

--
-- Índices de tabela `servicosagendados`
--
ALTER TABLE `servicosagendados`
  ADD PRIMARY KEY (`idServicosAgendados`),
  ADD KEY `fk_servicosAgendados_agendamento1_idx` (`idAgendamento`),
  ADD KEY `fk_servicosAgendados_servico1_idx` (`idServico`);

--
-- Índices de tabela `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idTipo`);

--
-- Índices de tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `fk_usuario_cidade1_idx` (`idCidade`),
  ADD KEY `fk_usuario_tipo1_idx` (`idTipo`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `agendamento`
--
ALTER TABLE `agendamento`
  MODIFY `idAgendamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de tabela `cidade`
--
ALTER TABLE `cidade`
  MODIFY `idCidade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de tabela `estado`
--
ALTER TABLE `estado`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de tabela `salao`
--
ALTER TABLE `salao`
  MODIFY `idSalao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `servico`
--
ALTER TABLE `servico`
  MODIFY `idServico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de tabela `servicosagendados`
--
ALTER TABLE `servicosagendados`
  MODIFY `idServicosAgendados` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `tipo`
--
ALTER TABLE `tipo`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `agendamento`
--
ALTER TABLE `agendamento`
  ADD CONSTRAINT `agendamento_ibfk_1` FOREIGN KEY (`idSalao`) REFERENCES `salao` (`idSalao`),
  ADD CONSTRAINT `fk_agendamento_usuario1` FOREIGN KEY (`idCabelereiro`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_agendamento_usuario2` FOREIGN KEY (`idCliente`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `cidade`
--
ALTER TABLE `cidade`
  ADD CONSTRAINT `fk_cidade_estado1` FOREIGN KEY (`idEstado`) REFERENCES `estado` (`idEstado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `salao`
--
ALTER TABLE `salao`
  ADD CONSTRAINT `fk_salao_cidade1` FOREIGN KEY (`idCidade`) REFERENCES `cidade` (`idCidade`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `servicosagendados`
--
ALTER TABLE `servicosagendados`
  ADD CONSTRAINT `fk_servicosAgendados_agendamento1` FOREIGN KEY (`idAgendamento`) REFERENCES `agendamento` (`idAgendamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_servicosAgendados_servico1` FOREIGN KEY (`idServico`) REFERENCES `servico` (`idServico`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_cidade1` FOREIGN KEY (`idCidade`) REFERENCES `cidade` (`idCidade`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_tipo1` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idTipo`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
