<?php

  //funcao que carrega o model dinamicamente pelo seu caminho
  function loadModel($modelName){
      require_once(MODEL_PATH . "/{$modelName}.php");
  }


                             
  function loadView($viewName , $params = array()){


    if(count($params) > 0){
        foreach($params as $key => $value){

          if(strlen($key) > 0){
              //gera variaveis com nomes das chaves
              //do array que recebe como params
              ${$key} = $value;
          }
        }
    }


    require_once(VIEW_PATH . "/{$viewName}.php");
  }




                             
  function loadTemplateView($viewName , $params = array()){


    if(count($params) > 0){
        foreach($params as $key => $value){

          if(strlen($key) > 0){
              //gera variaveis com nomes das chaves
              //do array que recebe como params
              ${$key} = $value;
          }
        }
    }

    require_once(TEMPLATE_PATH . "/header.php");
    require_once(TEMPLATE_PATH . "/menu.php"  );
    require_once(VIEW_PATH . "/{$viewName}.php");
    require_once(TEMPLATE_PATH . "/footer.php");

    
  }

  //no loader carrego a funcao render title e tambem o template que 
  // contem onde ira ficar o titulo e icone assim passando para o template os valores
  function renderTitle($title ,$icon = null){
    require_once(TEMPLATE_PATH . '/title.php');

  }