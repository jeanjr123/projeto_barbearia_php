<?php


error_reporting(0);
ini_set(“display_errors”, 0 );
session_start();
requireValidSession();

loadModel('Servico');

$exception = null;

$servicoResult = [];


if(count($_POST) === 0 && isset($_GET['update'])){


  $servico = Servico::getOne(['idServico' => $_GET['update']]);



  $servicoResult = $servico->getValues();

  
}else if(count($_POST) > 0){

  $_POST['preco'] = str_replace(',', '.' , $_POST['preco']);

  try{
      $dbService = new Servico($_POST);

      if($dbService->idServico){
        
        

        $dbService->update($dbService->idServico);
        addSuccessMsg('Servico alterado com sucesso!');
       
        ?>


        <script>
           setTimeout(function(){
               window.location.href = "servico.php";  
            }, 2000);       
        </script>

        <?php




      }else{
        
         
        $dbService->insertService();
        addSuccessMsg('Inserido com sucesso!');
       

      }
      $_POST = [];
    

  }catch(Exception $e){
      $exception = $e;
     
  }


}


loadTemplateView('cadServico', $servicoResult + ['exception' => $exception]);







