<?php
error_reporting(0);
ini_set(“display_errors”, 0 );

 loadModel('Login');

 session_start();
 $exception = null;


 if(count($_POST) > 0 ){
    //instancia login que esta na model carregada,
    //se ja existir no banco ele vai trazer o metodo magico get ao inves de set
    $login = new Login($_POST);

    try{

        $user =  $login->checkLogin();
        //setando usuario
        $_SESSION['user'] = $user;
        header("Location: home.php");
        //se tiver errado retorna erros da validation exception
        //como e extendida da appexception os erros sao instanciados nela
    }catch(AppException $e){
         
        $exception = $e;
        //voltando os erros ele manda para a messages.php
        //pois exception volta com um valor setado
        //e a pagina messages esta aguardando um valor 
        //pois foi carregada na view e a view esta aguardando o exception
    }

 }

 //passo o valor de exception como parametro para view
 loadView('login', $_POST + ['exception' => $exception] );


 