<?php


error_reporting(0);
ini_set(“display_errors”, 0 );

session_start();

requireValidSession();

loadModel('Barbearia');

if(isset($_GET['delete'])){
    try{

        Salao::deleteById($_GET['delete']);

        addSuccessMsg("Barbearia excluída com sucesso!");
        
    }catch(Exception $e){
        
        if(stripos($e->getMessage(), "FOREIGN KEY")){
       
            addErrorMsg('Não é possível excluir barbearia que possui agendas!');
     
         }else{ 
            $exception = $e;
         }
    }
}


$barbearias = Salao::getAllBarberShop('nomeFantasia', ['cidade' => 'salao', 'estado' => 'cidade']);





loadTemplateView('barbearia', ['exception' => $exception ,'barbearias' => $barbearias]);
  
  