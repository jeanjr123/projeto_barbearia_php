<?php

session_start();

requireValidSession();

loadModel('User');


if(isset($_GET['delete'])){
    try{

      User::deleteById($_GET['delete']);

      addSuccessMsg('Usuário excluído com sucesso!');


    }catch(Exception $e){

        if(stripos($e->getMessage(), "FOREIGN KEY")){
            addErrorMsg('Não é possível excluir usuário já incluso em agendas!');
        }else{
            $exception = $e;
        }
      
        

    }
}



loadModel('Cidade');

loadModel('Estado');

loadModel('Tipo');

                                         //obs segundo parametro e o inner join 
                                         //obs cidade se liga com usuario id cidade e assim por diante
$usuarios = User::getAllUsers([] ,'*','nomeUsuario',['cidade' => 'usuario', 'estado' => 'cidade', 'tipo' => 'usuario']);
                             //primeiro parametro e o order by              


loadTemplateView('user', ['usuarios' => $usuarios]);
