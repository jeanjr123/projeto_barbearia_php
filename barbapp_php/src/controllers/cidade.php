<?php


error_reporting(0);
ini_set(“display_errors”, 0 );

session_start();

requireValidSession();

loadModel('Cidade');

loadModel('Estado');

if($_GET['delete']){

  try{

    Cidade::deleteById($_GET['delete']);

    addSuccessMsg("Cidade excluída com sucesso!");

  }catch(Exception $e){

    if(stripos($e->getMessage(), "FOREIGN KEY")){
     
       addErrorMsg('Não é possível excluir cidade que possui agendas!');

    }else{ 
       $exception = $e;
    }
  }


}


$cidades = Cidade::getAllCityes('nomeCidade', ['estado' => 'cidade']);



loadTemplateView('cidade', ['exception' => $exception ,'cidades' => $cidades]);


