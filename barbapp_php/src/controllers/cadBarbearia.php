<?php


error_reporting(0);
ini_set(“display_errors”, 0 );
session_start();

requireValidSession();

loadModel('Barbearia');

loadModel('Cidade');


$cidades = Cidade::getAllCityes('nomeCidade',['estado'=> 'cidade']);


$exception = null;


$barbeariaResult = [];


if(count($_POST) === 0 && isset($_GET['update'])){


    $barbearia = Salao::getOne(['idSalao' => $_GET['update']]);
  
  
  
    $barbeariaResult = $barbearia->getValues();
  
    
}else if(count($_POST) > 0){

    try{

      
        $barbearia = new Salao($_POST);

        if($barbearia->idSalao){


        $barbearia->update($barbearia->idSalao);

        addSuccessMsg('Barbearia alterada com sucesso!');

        ?>
        <script>
           setTimeout(function(){
               window.location.href = "barbearia.php";  
            }, 2000);       
        </script>

        <?php

        }else{

            $barbearia->insertBarberShop();

            addSuccessMsg('Barbearia inserida com sucesso!');



        }

        
       
        $_POST = [];
  
    }catch(Exception $e){
        $exception = $e;
    }



}

loadTemplateView('cadBarbearia', $barbeariaResult + ['exception' => $exception , 'cidades' => $cidades]);