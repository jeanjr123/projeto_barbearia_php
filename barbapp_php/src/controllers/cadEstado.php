<?php


error_reporting(0);
ini_set(“display_errors”, 0 );
session_start();
requireValidSession();

loadModel('Estado');

$exception = null;

$estadoResult = [];

if(count($_POST) == 0 && isset($_GET['update'])){

   $estado = Estado::getOne(['idEstado' => $_GET['update']]);

   $estadoResult = $estado->getValues();


}if(count($_POST) > 0){
   try{
      //instancio passando o valor do post no construtor 
      $estado = new Estado($_POST);

      if($estado->idEstado){

          //chamo o metodo insert
      $estado->update($estado->idEstado);
      //chamo msg de sucesso
      addSuccessMsg('Estado atualizado com sucesso!');

      ?>
      <script>
         setTimeout(function(){
             window.location.href = "estado.php";  
          }, 2000);       
      </script>
      <?php



      }else{


      //chamo o metodo insert
      $estado->insertState();
      //chamo msg de sucesso
      addSuccessMsg('Estado inserido com sucesso!');



      }

      $_POST = [];


   }catch(Exception $e){
       $exception = $e;
   }

}

loadTemplateView('cadEstado', $estadoResult + ['exception' => $exception]);