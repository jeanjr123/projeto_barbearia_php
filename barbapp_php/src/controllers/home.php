<?php

error_reporting(0);
ini_set(“display_errors”, 0 );
session_start();

//valida usuario de sessao
requireValidSession();





loadModel('Agendamento');



//pegando data atual

$date = (new Datetime())->getTimeStamp();

$today = strftime('%d  de %B de %Y', $date);


$agendamento = Agendamento::loadFromDate(date('Y-m-d'));

$totalAgendamentos = 0;

foreach($agendamento as $ag){
    $totalAgendamentos++;
}



loadTemplateView('home', ['today' => $today,'agendamento' => $agendamento, 'totalAgendamentos' => $totalAgendamentos   ]);
