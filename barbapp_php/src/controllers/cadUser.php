<?php


error_reporting(0);
ini_set(“display_errors”, 0 );
session_start();

 requireValidSession();

 loadModel("User");

 loadModel("Cidade");

 loadModel('Barbearia');

 loadModel("Estado");

 loadModel("Tipo");

             
 $saloes = Salao::getAllBarberShop('nomeFantasia', []);

 $cidades = Cidade::getAllCityes('nomeCidade', ['estado' => 'cidade']);

 $tipos = Tipo::getAllTypes();


 $exception = null;

 $userResult = [];


 if(count($_POST) == 0 && isset($_GET['update'])){
   
     $user = User::getOne(['idUsuario' => $_GET['update']]);

     $user->senha = "";

     $userResult = $user->getValues();



 }else if(count($_POST) > 0){

    try{

        $dbUser = new User($_POST);

        if($dbUser->idUsuario){

            $dbUser->update($dbUser->idUsuario);

            addSuccessMsg('Usuário alterado com sucesso!');

            ?>


            <script>
               setTimeout(function(){
                   window.location.href = "user.php";  
                }, 2000);       
            </script>
    
            <?php


        }else{


        $dbUser->insertUser();

        addSuccessMsg('Usuário cadastrado com sucesso!');



        }



        $_POST = [];

    }catch(Exception $e){
        
        $exception = $e;
    }
 }




 loadTemplateView("cadUser",  $userResult + ['exception' => $exception ,'cidades' => $cidades, 'tipos' => $tipos, 'saloes' => $saloes]);

