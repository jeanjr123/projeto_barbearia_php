<?php

 error_reporting(0);
 ini_set(“display_errors”, 0 );
session_start();
requireValidSession();

loadModel('Cidade');
loadModel('Estado');
                           //nomeEstado = order by
$estados = Estado::getAllEstados('nomeEstado');


$cidadeResult = [];

if(count($_POST) === 0 && isset($_GET['update'])){


    $cidade = Cidade::getOne(['idCidade' => $_GET['update']]);
  

    $cidadeResult = $cidade->getValues();
  
    
}else if(count($_POST) > 0){

    try{
        
        $cidade = new Cidade($_POST);

        if($cidade->idCidade){
          

           $cidade->update($cidade->idCidade);

           addSuccessMsg('Cidade alterada com sucesso!');


           ?>
           <script>
              setTimeout(function(){
                  window.location.href = "cidade.php";  
               }, 2000);       
           </script>
           <?php
   

        }else{

            $cidade->insertCitye();

            addSuccessMsg('Cidade inserida com sucesso!');

        }

  

        $_POST=[];

    }catch(Exception $e){

        $exception = $e;
    }
  


}



loadTemplateView('cadCidade', $cidadeResult + ['exception' => $exception, 'estados' => $estados]);

