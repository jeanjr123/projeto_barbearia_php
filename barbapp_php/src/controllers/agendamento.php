<?php

error_reporting(0);
ini_set("display_errors", 0);
session_start();
requireValidSession();

loadModel('Agendamento');

loadModel("ServicosAgendados");

########################################### INSERT AGENDAMENTO #####################################



if($_GET['delete']){

    $id = $_GET['delete'];
        
    ServicosAgendados::deleteByWhere(" idAgendamento = $id");
        
    Agendamento::deleteById($id);

}








if ($_POST) {

    

    $idServicos = $_POST['idServicos'];

    $idServicos = explode(",",$idServicos);

    $_POST['idCliente'] = ($_SESSION['user']->idUsuario);

    $date = DateTime::createFromFormat("d/m/Y", $_POST['data']);

    $_POST['data'] = $date->format('Y-m-d');

    $_POST['horaInicio'] = $_POST['data'] . " " . $_POST['horaInicio'];

    $_POST['horaFim'] = $_POST['data'] . " " . $_POST['horaFim'];
}



$exception = null;

$agendamentoResult = [];

if (count($_POST) == 0 && isset($_GET['update'])) {

    $agendametno = Agendamento::getOne(['idAgendamento' => $_GET['update']]);

    $agendametnoResult = $agendametno->getValues();
}
if (count($_POST) > 0) {
    try {
        //instancio passando o valor do post no construtor 

        

        $agendamento = new Agendamento($_POST);

        if ($agendamento->idAgendamento) {


            //chamo o metodo insert
            $agendamento->update($agendamento->idAgendamento);
            //chamo msg de sucesso
            addSuccessMsg('Estado atualizado com sucesso!');

?>
            <script>
             
                    window.location.href = "agendamento.php";
               
            </script>
<?php



        } else {
            $servicos_agendados = [];
            //chamo o metodo insert
            
     
            $agendamento->insertAgendamento();

            $idLastAgendamento = $agendamento->getLastAgendamento();

    

            foreach($idServicos as $key => $value){
               $servicos_agendados['idAgendamento'] = $idLastAgendamento;
               
               $servicos_agendados['idServico'] = $value;

               $servicosAgendados = new ServicosAgendados($servicos_agendados);

               $servicosAgendados->insertServAgend();


       

            }
            


           
             
            

            //chamo msg de sucesso
            addSuccessMsg('Agendamento inserido com sucesso!');


            ?>
            <script>
               
                    window.location.href = "agendamento.php";
               
            </script>
            <?php


           
        }

        $_POST = [];
    } catch (Exception $e) {
        $exception = $e;
    }
}

###################################  VIEW PARAMETERS #################################################

loadModel("Agendamento");

loadModel("User");

loadModel("Servico");

loadModel("Barbearia");



$agendamentos = Agendamento::getAgendamentos('idAgendamento');

$servicos = [];

$idServicos = [];

$preco = 0;

foreach ($agendamentos as $key => $value) {


    $cliente = User::getOneUser($value->idCliente);

    $value->nomeCliente = $cliente->nomeUsuario;

    $cabelereiro = User::getOneUser($value->idCabelereiro);

    $value->nomeCabelereiro = $cabelereiro->nomeUsuario;

    $idServicos = ServicosAgendados::getAllServicosAgendados(
        ["idAgendamento" => $value->idAgendamento],
        'idAgendamento'
    );



    foreach ($idServicos as $key => $value2) {
        $servico = Servico::getOneService($value2->idServico);

        array_push($servicos, $servico->descricao . " ( R$ " . number_format($servico->preco, 2, ',', '.') . " )");

        $preco += $servico->preco;
    }


    $value->servicosCliente = implode(" / ", $servicos);

    $value->preco = $preco;

    $servicos = [];

    $preco = 0;
}

//informacoes para combo-box
//where         //columns                 //order by  //inner join
$barbeiros = User::getAllUsers(['idTipo = 2'], 'idUsuario, nomeUsuario', 'idUsuario', []);

$barbearias = Salao::getAllBarberShop('nomeFantasia', []);

$servicos = Servico::getAll('descricao', []);












loadTemplateView(
    "agendamento",
    [
        'agendamentos' => $agendamentos,
        'barbeiros' => $barbeiros,
        'barbearias' => $barbearias,
        'servicos' => $servicos
    ]
    
);
