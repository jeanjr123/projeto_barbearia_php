<?php

// error_reporting(0);
// ini_set(“display_errors”, 0 );
session_start();


requireValidSession();

loadModel('Servico');

if(isset($_GET['delete'])){
    

    try{

        Servico::deleteById($_GET['delete']);
    
        addSuccessMsg("Serviço excluido com sucesso!");
    
    }catch(Exception $e){
    
        if(stripos($e->getMessage() , "FOREIGN KEY")){

         addErrorMsg('Não é possível excluir servico incluso em agendas!');
     
        


        }else{

            $exception = $e;
        }
    
    
    }
    
}


$servicos = Servico::getAll();


loadTemplateView('servico', ['servicos' => $servicos]);


