<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v3.0/admin/html/page_with_mega_menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Mar 2018 17:24:30 GMT -->

<head>
    <meta charset="utf-8" />
    <title>BarbApp</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet" />
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <link href="assets/css/style-responsive.min.css" rel="stylesheet" />
    <link href="assets/css/theme/default.css" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- PLUGINS MASK -->
    <script src="assets/plugins/ajax/jquery.min.js"></script>
    <script src="assets/plugins/jquery.mask/jquery.mask.min.js"></script>

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="assets/plugins/pace/pace.min.js"></script>
    <script src="assets/plugins/bootstrap/js/sweetalert.min.js"></script>
    <!-- ================== END BASE JS ================== -->

    <!-- FULL CALENDAR -->

    <link href='assets/css/core/main.min.css' rel='stylesheet' />
    <link href='assets/css/daygrid/main.min.css' rel='stylesheet' />
    <link href='assets/css/timegrid/main.min.css' rel='stylesheet' />
    <link href='assets/css/list/main.min.css' rel='stylesheet' />

    <script src='assets/js/core/main.min.js'></script>
    <script src='assets/js/interaction/main.min.js'></script>
    <script src='assets/js/daygrid/main.min.js'></script>
    <script src='assets/js/timegrid/main.min.js'></script>
    <script src='assets/js/list/main.min.js'></script>
    <script src='assets/js/core/locales/pt-br.js'></script>




    <style>
        .centralizar {
            margin-top: 4px;
        }
    </style>
</head>

<body>


    <?php


    error_reporting(0);
    ini_set(“display_errors”, 0);
    session_start();

    //se for primeiro cadastro nao valido sessao
    //requireValidSession();

    loadModel("User");

    loadModel("Cidade");

    loadModel('Barbearia');

    loadModel("Estado");

    loadModel("Tipo");


    $saloes = Salao::getAllBarberShop('nomeFantasia', []);

    $cidades = Cidade::getAllCityes('nomeCidade', ['estado' => 'cidade']);

    $tipos = Tipo::getAllTypes();


    $exception = null;

    $userResult = [];


    if (count($_POST) == 0 && isset($_GET['update'])) {

        $user = User::getOne(['idUsuario' => $_GET['update']]);

        $user->senha = "";

        $userResult = $user->getValues();
    } else if (count($_POST) > 0) {

        try {

            $dbUser = new User($_POST);

            if ($dbUser->idUsuario) {

                $dbUser->update($dbUser->idUsuario);

                addSuccessMsg('Usuário alterado com sucesso!');
            ?>
                <script>
                    setTimeout(function() {
                        window.location.href = "user.php";
                    }, 2000);
                </script>

            <?php


            } else {


                $dbUser->insertUser();

                addSuccessMsg('Usuário cadastrado com sucesso!');
            ?>
                <script>
                    setTimeout(function() {
                        window.location.href = "index.php";
                    }, 2000);
                </script>
           <?php
            }

            $_POST = [];
        } catch (Exception $e) {

            $exception = $e;
        }
    }




    loadView("cadFirstUser",  $userResult + ['exception' => $exception, 'cidades' => $cidades, 'tipos' => $tipos, 'saloes' => $saloes]);

    ?>



    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
    <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
    <!-- ================== END BASE JS ================== -->

    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="assets/js/apps.min.js"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->

    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../../../www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53034621-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>

<!-- Mirrored from seantheme.com/color-admin-v3.0/admin/html/page_with_mega_menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Mar 2018 17:24:30 GMT -->

</html>