<?php

error_reporting(0);
ini_set(“display_errors”, 0 );

session_start();
requireValidSession();
loadModel('Estado');

if($_GET['delete']){

    try{

        Estado::deleteById($_GET['delete']);

        addSuccessMsg("Estado excluída com sucesso!");


    }catch(Exception $e){
        

        if(stripos($e->getMessage(), "FOREIGN KEY")){

            addErrorMsg("Não é possível excluir estado que possui agendas!");

        }else{

            $exception = $e;
        }
        
    }


}


           //nomeEstado = order by
$estados = Estado::getAllEstados('nomeEstado');

loadTemplateView('estado', ['exception' => $exception ,'estados' => $estados]);

