<?php

class ValidationException extends AppException{
  
    private $errors = [];

    public function __construct(
        $errors = [],
        $message = 'Erros de validação',
        $code = 0,$previous = null
        ){
       
        //executa o construtor da classe que foi extendida
        parent::__construct($message, $code, $previous);

        //errors vai vir da instancia da classe validationexception
        $this->errors = $errors;
    }

     //metodo get errors retorna o array com todos os erros
     public function getErrors(){
         return $this->errors;
     }

     //metodos get retorna um erro especifico
     public function get($att){
         return $this->errors[$att];
     }



}