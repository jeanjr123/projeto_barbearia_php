<?php

class Estado extends Model{
    protected static $tableName='estado';
    protected static $columns =[
      'idEstado',
      'nomeEstado',
      'sigla'
    ];



    public function getAllEstados($order){
        
        $estados = self::get([] ,'*',$order);

        return $estados;

    }


    public function getOneState($idEstado){

        $estado = self::getOne(['idEstado' => $idEstado]);

        return $estado;
    }


    public function insertState(){

        if($this->idEstado){
            $this->update();
        }else{
            $this->validate();

            $this->insert();
        }
   
   
     }


    public function validate(){

        $errors = [];

        if(!$this->nomeEstado){
            $errors['nomeEstado'] = "Estado é um campo obrigatório!";
        }

        if(!$this->sigla){
            $errors['sigla'] = "Sigla é um campo obrigatório!";
        }


        if(count($errors) > 0){

            throw new ValidationException($errors);

        }



    }
   

}