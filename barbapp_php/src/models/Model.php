<?php

class Model {

    protected static $tableName = '';
    protected static $columns = [];
    protected $values = [];


   //construct
    public function __construct($arr, $sanitize = true){
       $this->loadFromArray($arr, $sanitize);
    }
  
    public function loadFromArray($arr, $sanitize = true){
        
        if($arr){
            foreach($arr as $key => $value){

              $cleanValue = $value;

              if($sanitize && isset($cleanValue)){

                //tira as tags html
                $cleanValue = strip_tags(trim($cleanValue));
                //tranformma o html em html entities ex nbsp e etc
                // no cotes ele nao mexe nas <  >
                $cleanValue = htmlentities($cleanValue, ENT_NOQUOTES);
                        //para nao ter mysql injection
               // $cleanValue = mysqli_real_escape_string($conn, $cleanValue);

 
              }
            
               $this->$key = $cleanValue;
            }
        }
    }

    //geter and seter, magic methods

    public function __get($key){
        return $this->values[$key];
    }



    public function __set($key, $value){
        $this->values[$key] = $value;
    }

    //retorna os values que estao armazenados no array interno
    //this->values imprime o atributo values
    public function getValues(){
        return $this->values;
    }


     //public method can be access for instance
    public static function get($filters = [],$columns = '*', $order = "", $inner = []){
            $objects=[];
            $result = static::getResultSetFromSelect($filters , $columns, $order, $inner);
            //method show called class
            $class = get_called_class();
           
            
           
            if($result){

                while($row = $result->fetch_assoc()){
                    //construct instance class 
                  //classe sera instanciada com resultados do select
                array_push($objects, new $class($row));
                }

         
            }

           

            return $objects;

    }


    public static function getOne($filters = [] , $columns = '*'){
         $class = get_called_class();

         $result = static::getResultSetFromSelect($filters, $columns);

         return $result ? new $class($result->fetch_assoc()): null;
    }

    
    

     //parameter filter -> clausula where
    public static function getResultSetFromSelect($filters = [] ,$columns = '*', $order = "", $inner = []){
        $sql = "SELECT ${columns} FROM " 
       . strtolower(static::$tableName);

       if($inner != ""){
           
         foreach($inner as $key => $value){
            $sql .= " INNER JOIN $key ON  {$key}.id{$key} = ";
            $sql .= strtolower($value);
            $sql .= ".id{$key}";
         }
       }

       $sql .=  static::getFilters($filters);

       if($order != ""){

           $sql .= " order by ". $order . " asc";

           //echo $sql; die();
   
       }
        
              

        $result = Database::getResultFromQuery($sql);

       

        
        if($result->num_rows === 0){
            return null;
        }else{
            return $result;
        }


    }


    public function insert(){
                //static porque esta pegando valor static das tabelas
        $sql = "INSERT INTO " . strtolower(static::$tableName) . " ("

        . implode(",", static::$columns) . ") VALUES (" ;

        foreach(static::$columns as $col){
            
                //this col porque e um atributo de instancia 
        $sql .= static::getFormatedValue($this->$col) . "," ;

        }
        //para tirar uma virgula que esta sobrando na query
        $sql[strlen($sql) - 1] = ')';

     // echo $sql;   die();


        $id = Database::executeSql($sql);

        //seto o id na instancia atual
        $this->id = $id;
    }



    public function update($id){


                        //uso atributo estatico da instancia
        $sql = "UPDATE " . strtolower(static::$tableName) . " SET ";

        foreach(static::$columns as $col){ 
                    //coluna                   //valor de col da instancia
            $sql .= " ${col} = " .static::getFormatedValue($this->$col) . ",";
        }

        $sql[strlen($sql) - 1] = " ";

        $sql .= "WHERE id".static::$tableName. " = " .$id;


      
        Database::executeSQL($sql);


    }


     
     // must be static for getResultFromSelect to access this method
     // private restricted to own class and not transmitted by inheritance
    private static function getFilters($filters){
        $sql = '';
        if(count($filters) > 0){ 
            $sql .= " WHERE 1 = 1 ";

            foreach($filters as $column => $value){

                if($column == 'raw'){
                    $sql .= "AND {$value}";
                 }
           
            $sql .= " AND ${column} = " . static::getFormatedValue($value);
            }
        }


        return $sql;
    }

     // method to format the filter according to its type
     // private can only be accessed by this class, and static is a class variable having the same value for all your instances
    private static function getFormatedValue($value){
         if(is_null($value)){
             return "null";
         }elseif(gettype($value) === 'string'){
             return "'${value}'";

         }else{
             return $value;
         }
    }


    public static function deleteById($id){
      
        $sql = "DELETE FROM " . static::$tableName . " WHERE id" .static::$tableName. " = {$id}";

        $sql = strtolower($sql);

        Database::executeSQL($sql);


    }


    public static function deleteByWhere($where){
      
        $sql = "DELETE FROM " . static::$tableName . " WHERE " . $where;

       // echo $sql;  die();

        $sql = strtolower($sql);

        Database::executeSQL($sql);


    }






}