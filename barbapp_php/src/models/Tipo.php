<?php 

class Tipo extends Model{
    protected static $tableName = 'tipo';
    protected static $columns = [
        'idTipo',
        'descricaoTipo'
    ];


    public function getOneType($idTipo){
        
        $tipo = self::getOne(['idTipo' => $idTipo]);

        return $tipo;

    }


    public function getAllTypes(){
      
        $types = self::get();



        if(!$types){
            $types = new Servico([
            'idTipo' => '',
            'descricaoTipo' => ''
            ]);
        }


        return $types;

    }




}