<?php

class Salao extends Model{
     protected static $tableName = 'salao';
     protected static $columns = [
        'idSalao', 
        'razaoSocial',
        'nomeFantasia',
        'cnpj', 
        'inscricaoEstadual',
        'telefone1',
        'telefone2', 
        'enderecoLogradouro', 
        'numero', 
        'bairro',
        'cep',
        'email',
        'idCidade'
     ];


     public function getAllBarberShop($order, $inner){

       $barbearias = self::get([],'*', $order, $inner);
 
       return $barbearias;

     }

     public function insertBarberShop(){
      if($this->idSalao){
          self::update();
      }else{
          $this->validate();

          self::insert();
      }
   }



    private function validate(){

      $errors = [];

      if(!$this->razaoSocial){
        $errors['razaoSocial'] = "Razão Social é um campo obrigatório!";
      }


      if(!$this->nomeFantasia){
        $errors['nomeFantasia'] = "Nome fantasia é um campo obrigatório!";
      }


      if(!$this->cnpj){
        $errors['cnpj'] = "Cnpj é um campo obrigatório!";
      }


      if(!$this->inscricaoEstadual){
        $errors['inscricaoEstadual'] = "Inscrição estadual é um campo obrigatório!";
      }



      if(!$this->telefone1){
        $errors['telefone1'] = "Celular é um campo obrigatório!";
      }



      if(!$this->telefone2){
        $errors['telefone2'] = "Telefone Fixo é um campo obrigatório!";
      }



      if(!$this->email){
        $errors['email'] = "Email é um campo obrigatório!";
      }

      if(!$this->cep){
        $errors['cep'] = "Cep é um campo obrigatório!";
      }


      if(!$this->enderecoLogradouro){
        $errors['enderecoLogradouro'] = "Logradouro é um campo obrigatório!";
      }

      if(!$this->numero){
        $errors['numero'] = "Numero é um campo obrigatório!";
      }

      if(!$this->bairro){
        $errors['bairro'] = "Bairro é um campo obrigatório!";
      }

      if(!$this->idCidade){
        $errors['idCidade'] = "Cidade é um campo obrigatório!";
      }



      if(count($errors) > 0){

        throw new ValidationException($errors);

      }

    }

}