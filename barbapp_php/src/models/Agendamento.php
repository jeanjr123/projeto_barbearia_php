<?php

class Agendamento extends Model {
    protected static $tableName = 'agendamento';
    protected static $columns = [
      'idAgendamento',
      'data',
      'horaInicio',
      'horaFim',
      'idCabelereiro',
      'idCliente',
      'idSalao'
    ] ;


    public static function loadFromDate($data){

      
                //self porque estou usando um metodo da classe que esta sendo extendida 
      $agendamento = self::get(['data' => $data]);

    

      if(!$agendamento){
         $agendamento = new Agendamento([
          'idAgendamento' => '',
          'data' => '',
          'horaInicio' => '',
          'horaFim' => '',
          'idCabelereiro' => '',
          'idCliente' => ''
         ]);
      }
    
       
      return $agendamento;
 
    
    }


  public function getAgendamentos($order){
         
      $agendamentos = self::get([],'*', $order);

      return $agendamentos;

  }




  public function getOneAgendamento($idAgendamento){

      $agendamento= self::getOne(['idAgendamento' => $idAgendamento]);

      return $agendamento;
  }





  public function insertAgendamento(){
    

    if($this->idAgendamento){
     
         $this->update();
    }else{
        
  
        $this->insert();
    }
  }


  public function getLastAgendamento(){
         
   $lastAgendamento =  self::get([], 'idAgendamento', "idAgendamento", []);

   $lastAgendamento = $lastAgendamento[count($lastAgendamento) - 1]->idAgendamento;   

   return $lastAgendamento;


  }

}