<?php


class Cidade extends Model {

    protected static $tableName='cidade';
    protected static $columns = [
       'idCidade',
       'nomeCidade',
       'idEstado'   
    ];



    public function getAllCityes($order, $inner){
         
        $cidades = self::get([],'*', $order, $inner);

        return $cidades;

    }

    public function getOneCitye($idCidade){

        $cidade = self::getOne(['idCidade' => $idCidade]);

        return $cidade;
    }

    
    public function insertCitye(){
       if($this->idCidade){
           self::update();
       }else{
           $this->validate();

           self::insert();
       }
    }


    private function validate(){

        $errors = [];

        if(!$this->nomeCidade){

            $errors['nomeCidade'] = "Cidade é um campo obrigatório!";
    
        }


         if(!$this->idEstado){
            
             $errors['idEstado'] = "Estado é um campo obrigatório!";

         }



      if(count($errors) > 0){

        throw new ValidationException($errors);

      }

    }


}