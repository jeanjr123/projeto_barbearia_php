<?php
class Login extends Model {

    public function validate(){

      $errors = [];

      if(!$this->login){

         $errors['login'] = 'Por favor, informe o login.';

      }

      if(!$this->senha){
      
         $errors['senha'] = 'Por favor, informe a senha.';

      }


      if(count($errors) > 0){
         //se nao tiver setado login ou senha vai instanciar validation exception
         //com os errors como parametro, entrando assim no catch do controler
         // que checa login
         throw new ValidationException($errors);


      }


    }




    public function checkLogin(){

      $this->validate();
     //variavel user recebe o valor do metodo de classe getOne
     //recebe pelo user pois ele extend model
     $user = User::getOne(['login' => $this->login]);

     if($user){
          //se tiver inativo
          if($user->status != 'ATIVO'){
             throw new AppException('Usuario Inativo');
          }
          
           //verifica se password da instancia login (que vem do form)
           // e igual ao do banco buscado pelo get one
          if(password_verify($this->senha, $user->senha)){

             return $user;

          }
          throw new AppException('Usuario e senha invalidos!');

     }

    
    }

}