<?php

class Servico extends Model{
 
    protected static $tableName = 'Servico';
    protected static $columns = [
      'idServico',
      'descricao',
      'tempoEstimado',
      'preco'
    ];




public static function getAll(){
      
    $servicos = self::get();


    if(!$servicos){
        $servicos = new Servico([
        'idServico' => '',
        'descricao' => '',
        'tempoEstimado' => '',
        'preco' => ''
        ]);
    }


    return $servicos;


}



public function getOneService($idServico){

    $servico = self::getOne(['idServico' => $idServico]);

    return $servico;
}


  public function insertService(){

     if($this->id){
         $this->update();
     }else{
         $this->validate();

         $this->insert();
     }


  }




  private function validate(){

     $errors = [];

     if(!$this->descricao){
        $errors['descricao'] = "Descrição Serviço é um campo Obrigatório";
     }

     if(!$this->tempoEstimado){
        $errors['tempoEstimado'] = "Tempo Estimado é um campo Obrigatório";
     }


     if(!$this->preco){
        $errors['preco'] = "Preço Serviço é um campo Obrigatório";
     }

   
     if(count($errors) > 0){
         throw new ValidationException($errors);
     }



  }






}