<?php



class User extends Model{

   protected static $tableName = 'usuario';
   protected static $columns = [
    'idUsuario',
    'login', 
    'nomeUsuario', 
    'cpf',
    'rg', 
    'enderecoLogradouro',
    'numero',
    'bairro',
    'cep',
    'celular', 
    'telefoneFixo',
    'dataNasc',
    'email', 
    'senha', 
    'status', 
    'idCidade', 
    'idTipo'
   ];


   public function getAllUsers($where,$columns,  $order, $inner){

      $usuarios = self::get($where ,$columns, $order, $inner);

      return $usuarios;


   }

   public function getOneUser($idUsuario){

    $usuario = self::getOne(['idUsuario' => $idUsuario]);

    return $usuario;
}




   public function insertUser(){
      if($this->idUsuario){
          
          $this->update();
      }else{

          $this->validate();

          $this->senha = password_hash($this->senha, PASSWORD_DEFAULT);

          $this->insert();
      }
   }



   private function validate(){

    $errors = [];

    if(!$this->nomeUsuario){

      $errors['nomeUsuario'] = "Nome é um campo obrigatorio!";

    }


    if(!$this->login){

      $errors['login'] = "Login é um campo obrigatorio!";

    }


    if(!$this->cpf){

      $errors['cpf'] = "Cpf é um campo obrigatorio!";

    }


    if(!$this->rg){

      $errors['rg'] = "Rg é um campo obrigatorio!";

    }


    if(!$this->celular){

      $errors['celular'] = "Celular é um campo obrigatorio!";

    }

    if(!$this->telefoneFixo){

      $errors['telefoneFixo'] = "Telefone Fixo é um campo obrigatorio!";

    }

    if(!$this->email){

      $errors['email'] = "Email é um campo obrigatorio!";

    }

    if(!$this->senha){

      $errors['senha'] = "Senha é um campo obrigatorio!";

    }

    if(!$this->confirme_senha){

      $errors['confirme_senha'] = "Confirmar senha é um campo obrigatorio!";

    }

    if(!$this->dataNasc){

      $errors['dataNasc'] = "Data de Nascimento é um campo obrigatorio!";

    }


    if(!$this->cep){

      $errors['cep'] = "Cep é um campo obrigatorio!";

    }


    if(!$this->enderecoLogradouro){

      $errors['enderecoLogradouro'] = "Logradouro é um campo obrigatorio!";

    }


    if(!$this->numero){

      $errors['numero'] = "Numero é um campo obrigatorio!";

    }


    if(!$this->bairro){

      $errors['bairro'] = "Bairro é um campo obrigatorio!";

    }


    if(!$this->idCidade){

      $errors['idCidade'] = "Cidade é um campo obrigatorio!";

    }

    if(!$this->idTipo){

      $errors['idTipo'] = "Tipo é um campo obrigatorio!";

    }

    if(!$this->status){

      $errors['status'] = "Status é um campo obrigatorio!";

    }


    if(($this->senha && $this->confirme_senha) &&
    ($this->senha != $this->confirme_senha)){
      
     $errors['senha'] = "As senhas nao sao iguais!";

     $errors['confirme_senha'] = "As senhas nao sao iguais!";

    }





    if(count($errors) > 0){

      throw new ValidationException($errors);

    }


   }

}