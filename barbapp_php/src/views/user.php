	
		<!-- begin #content -->
		<div id="content" class="content">

		
			<!-- begin breadcrumb --->
			<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
			<!-- end page-header -->
			
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar" ></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			        </div>
					<?php      
					//chamando a funcao que foi carregada no loader
					renderTitle(
					 "Usuários  Cadastrados"
					)				
					?>
			    </div>
				<div class="panel-body">
                <?php if($_SESSION['user']->idTipo == 2){ ?>
				<a class="btn btn-primary" style="margin-bottom:-10px" href="cadUser.php">
				<i class="fa fa-plus"></i> Novo Usuário</a>
				<?php } ?>
				<?php   include(TEMPLATE_PATH . '/messages.php');   ?>
				<hr>
				    
				    <ul class="media-list me</div>dia-list-with-divider">
							   <?php  foreach($usuarios as $key => $value){ ?>
								<?php if($_SESSION['user']->idTipo == 2
								|| ($_SESSION['user']->idTipo == 1 && $value->idUsuario == $_SESSION['user']->idUsuario)){ ?>
								<li class="media media-sm">
									<a class="media-left" href="javascript:;">
									    <img src="assets/img/user-5.jpg" alt="" class="media-object rounded-corner" />
									</a>
									
									<div class="table-responsive">
									<table >
										<thead>
											<th></th>
										</thead>
										<tbody>
                                        <tr>
										<td>
									<div class="media-body">
										<h4 class="media-heading"><strong><?= ucwords(strtolower($value->nomeUsuario))  ?></strong>&nbsp;&nbsp;  </h4>
										<h5><strong>Tipo Usuário:&nbsp;&nbsp;</strong><?= ucwords(strtolower($value->descricaoTipo)) ?></h5>
										<h5><strong>Login: </strong><?= $value->login ?>&nbsp;&nbsp;<strong>Rg: </strong><?= $value->rg ?>&nbsp;&nbsp;<strong>Cpf: </strong><?= $value->cpf ?> </h5>
										<h5><strong>Data Nasc: </strong><?= $value->dataNasc ?>&nbsp;&nbsp;<strong>Celular: </strong><?= $value->celular ?>&nbsp;&nbsp;<strong>Tel Fixo: </strong><?= $value->telefoneFixo ?></h5>
										<h5><strong>Email:   </strong><?= $value->email ?>&nbsp;&nbsp;<strong>Situacao: </strong><?= $value->status ?>&nbsp;&nbsp;</h5>
									    <h5><strong>Logradouro:   </strong><?= ucwords(strtolower($value->enderecoLogradouro))?>&nbsp;&nbsp;<strong>Nro: </strong><?= $value->numero ?>&nbsp;&nbsp;<strong>Bairro: </strong><?= ucwords(strtolower($value->bairro)) ?></h5>
		                                <h5><strong>Cep:   </strong><?= $value->cep ?>&nbsp;&nbsp;<strong>Cidade: </strong><?= ucwords(strtolower($value->nomeCidade)) . " / " . strtoupper($value->sigla) ?></h5>
										<p>
										<a href="cadUser.php?update=<?= $value->idUsuario  ?>" class="btn btn-warning"><i class="fa fa-pencil"></i> Editar</a>
										<?php if($_SESSION['user']->idTipo == 2){ ?>
										<button class="btn btn-danger" onclick="confirmar('<?= $value->idUsuario ?>' , 
										 '<?= ucwords(strtolower($value->nomeUsuario)) ?>')"><i class="fa fa-trash"></i> Excluir</button>   
										<?php } ?> 
									    </p>
									</div>
									</td>
									</tr>
									</tbody>
									</table>
									</div>
								</li>
							   <?php } }?>		
                    </ul>     
                </div>
			</div>
		</div>

		<script>
	   
	   function confirmar(idUser, nomeUser){

		   swal({
			   title: "Deseja realmente excluir o usuário " + nomeUser +" ?",
			   text: "",
			   icon: "warning",
			   buttons: ['cancelar', 'ok'],
			   dangerMode: true,
			   })
			   .then((willDelete) => {
			   if (willDelete) {
				   
				   window.location.href = "?delete="+idUser;

			   } else {
				   
				   return null;
			   }
			   });
		
		   return false;
			

	   }
	  
	  </script>

	





                       
                         