
		<!-- begin #content -->
		<div id="content" class="content">
		
			<!-- begin breadcrumb --->
			<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
			<!-- end page-header -->
			
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar" ></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			        </div>
					<?php      
					//chamando a funcao que foi carregada no loader
					renderTitle(
					 "Cadastro de Barbearias"
                    );
                    
					?>
			    </div>

				<div class="panel-body">
              
                <form action="cadBarbearia.php" method="POST">
                                <fieldset>
                                <?php   include(TEMPLATE_PATH . '/messages.php');   ?>

                                
                                            <legend><i class="fa fa-building"></i> Barbearia</legend>
                                            
                                  <?php  if(isset($_GET['update'])){ ?>  
                                    <input type="hidden" name="idSalao" value="<?= $_GET['update'] ?>">
                                  <?php } ?>

                                    <div class="form-group col-md-6">
                                        <label for="razaoSocial">Razão Social</label>
                                        <input type="text" class="form-control" id="razaoSocial" name="razaoSocial" 
                                        placeholder="Insira a razao social" <?= $erros['razaoSocial'] ? 'is_invalid' : '' ?>
                                        value = "<?= $razaoSocial  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['razaoSocial'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="nomeFantasia">Nome Fantasia</label>
                                        <input type="text" class="form-control" id="nomeFantasia" name="nomeFantasia" 
                                        placeholder="Insira o nome fantasia" <?= $erros['nomeFantasia'] ? 'is_invalid' : '' ?>
                                        value = "<?= $nomeFantasia  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['nomeFantasia'] ?>
                                         </div>
                                       
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="cnpj">CNPJ</label>
                                        <input type="text" class="form-control" id="cnpj" name="cnpj" 
                                        placeholder="Insira o cnpj" <?= $erros['cnpj'] ? 'is_invalid' : '' ?>
                                        value = "<?= $cnpj  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['cnpj'] ?>
                                         </div>
                                    </div>


                                    <div class="form-group col-md-6">
                                        <label for="inscricaoEstadual">Inscrição Estadual</label>
                                        <input type="text" class="form-control" id="inscricaoEstadual" name="inscricaoEstadual"
                                         placeholder="Insira a inscrição estadual" 
                                         <?= $erros['inscricaoEstadual'] ? 'is_invalid' : '' ?>
                                        value = "<?= $inscricaoEstadual  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['inscricaoEstadual'] ?>
                                         </div>
                                    </div>


                                    <div class="form-group col-md-6">
                                        <label for="telefone1">Celular</label>
                                        <input type="text" class="form-control" id="telefone1" name="telefone1" 
                                        placeholder="Insira o celular" <?= $erros['telefone1'] ? 'is_invalid' : '' ?>
                                        value = "<?= $telefone1  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['telefone1'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="telefone2">Telefone fixo</label>
                                        <input type="text" class="form-control" id="telefone2" name="telefone2" 
                                        placeholder="Insira o telefone fixo" <?= $erros['telefone2'] ? 'is_invalid' : '' ?>
                                        value = "<?= $telefone2  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['telefone2'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" 
                                        placeholder="Insira o email" <?= $erros['email'] ? 'is_invalid' : '' ?>
                                        value = "<?= $email  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['email'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="cep">CEP</label>
                                        <input type="text" class="form-control cep" id="cep" name="cep" 
                                        placeholder="Insira o cep" <?= $erros['cep'] ? 'is_invalid' : '' ?>
                                        value = "<?= $cep ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['cep'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="enderecoLogradouro">Logradouro</label>
                                        <input type="text" class="form-control" id="enderecoLogradouro" 
                                        name="enderecoLogradouro" placeholder="Insira o logradouro ex: Rua , Avenida.." 
                                        <?= $erros['enderecoLogradouro'] ? 'is_invalid' : '' ?>
                                        value = "<?= $enderecoLogradouro  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['enderecoLogradouro'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="numero">Numero</label>
                                        <input type="text" class="form-control" id="numero" name="numero" 
                                        placeholder="Insira o numero" <?= $erros['numero'] ? 'is_invalid' : '' ?>
                                        value = "<?= $numero  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['numero'] ?>
                                         </div>
                                    </div>


                                    <div class="form-group col-md-6">
                                        <label for="bairro">Bairro</label>
                                        <input type="text" class="form-control" id="bairro" name="bairro" 
                                        placeholder="Insira o bairro" <?= $erros['bairro'] ? 'is_invalid' : '' ?>
                                        value = "<?= $bairro  ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['bairro'] ?>
                                         </div>
                                    </div>

                               <!--  TEM QUE USAR API CORREIOS -->     


                                    <div class="form-group col-md-6">
                                    <label for="estado">Cidade</label>

                                   
                                        <select class="form-control" id="cidade" name="idCidade" 
                                         <?= $erros['idCidade'] ? 'is_invalid' : '' ?> >
                                         <option value="">Selecione a cidade</option>
                                            <?php   foreach($cidades as $key => $value){   ?>       
                                            <option value="<?= $value->idCidade ?>"><?= ucwords(strtolower($value->nomeCidade)) . " / " . strtoupper($value->sigla)?></option>
                                            <?php  } ?>
                                        </select>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['idCidade'] ?>
                                         </div>
                                    </div>                                                                                      
                                </fieldset>
                                       <div class="col-md-10">
                                            <button type="submit" class="btn btn-sm btn-success">Cadastrar</button>
                                        </div>
                            </form>
				       
                </div>
			</div>
        </div>

    <script type="text/javascript">
        $("#telefone1").mask("(00) 00000-0000");
        $("#telefone2").mask("(00) 0000-0000");
        $("#cnpj").mask("00.000.000/0000-00");
        $("#inscricaoEstadual").mask("000.000.000.000");
        $("#cep").mask("00.000-000");



         $(document).on("blur",".cep", function(){

            var cep = $("#cep").val();

            $.ajax({
               url: "/apiCorreios.php", 
              type: "POST" ,
              datatype: "json",
              data:"cep="+ cep,
              success: function(data){
                  var jsonData = JSON.parse(data);
                   $('#enderecoLogradouro').val(jsonData.logradouro);

                   $('#bairro').val(jsonData.bairro);
               }
               
            });


         })
    </script>
        
    
        <!-- end #content -->
        
