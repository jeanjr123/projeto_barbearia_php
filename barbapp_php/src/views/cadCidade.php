	
		<!-- begin #content -->
		<div id="content" class="content">
        

		
			<!-- begin breadcrumb --->
			<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
			<!-- end page-header -->
			
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar" ></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			        </div>
					<?php      
					//chamando a funcao que foi carregada no loader
					renderTitle(
					 "Cadastro de Cidades"
					)				
					?>
			    </div>
				<div class="panel-body">
                <form action="cadCidade.php" method="POST">
                                <fieldset>
								<?php   include(TEMPLATE_PATH . '/messages.php');  ?>
								

							
                                   
                                    <legend><i class="fa fa-university"></i> Cidade </legend>
                                   
								   <?php if(isset($_GET['update'])){ ?>
								     <input type="hidden" name="idCidade" value="<?= $_GET['update']?>">
								   <?php } ?>

                                    <div class="form-group col-md-6">
                                        <label for="nomeCidade">Nome Cidade</label>
                                        <input type="text" class="form-control" id="nomeCidade" name="nomeCidade" 
										placeholder="Insira o nome da cidade"
										 <?= $errors['nomeCidade'] ? 'is_invalid' : '' ?> 
										 value="<?= $nomeCidade ?>"/>
										 <div class="invalid-feedback" style="color: red">
                                            <?= $errors['nomeCidade'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                    <label for="estado">Estado</label>
                                        
                                        <select class="form-control" id="estado" name="idEstado"
										  <?= $errors['idEstado'] ? 'is_invalid' : '' ?>>
										    <option value = "">Selecione um estado</option>
                                            <?php   foreach($estados as $key => $value){ ?>
                                            <option value="<?= $value->idEstado ?>"><?= ucwords(strtolower($value->nomeEstado)) ?></option>
                                            <?php  } ?>
                                        </select>
										    <div class="invalid-feedback" style="color: red">
											   <?= $errors['idEstado'] ?>
											</div>
                                  
                                    </div>                                       
                                                                    
                                </fieldset>
                                       <div class="col-md-10">
                                            <button type="submit" class="btn btn-sm btn-success">Cadastrar</button>
                                        </div>
                            </form>
				       
                </div>
			</div>
        </div>
        
    
        <!-- end #content -->
        
