	
		<!-- begin #content -->
		<div id="content" class="content">

		
			<!-- begin breadcrumb --->
			<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
			<!-- end page-header -->
			
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar" ></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			        </div>
					<?php      
					//chamando a funcao que foi carregada no loader
					renderTitle(
					 "Servicos Cadastrados"
					)				
					?>
			    </div>
				<div class="panel-body">

				<a class="btn btn-primary" style="margin-bottom:15px" href="cadServico.php">
                <i class="fa fa-plus"></i> Novo Serviço</a>
				<?php   include(TEMPLATE_PATH . '/messages.php');   ?>

                  <div class="table-responsive">
                  <table id="data-table" class="table table-striped table-bordered nowrap table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Descrição Serviço</th>
                                        <th>Tempo Estimado</th>
                                        <th>Preço</th>
                                        <th>Editar</th>
                                        <th>Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php foreach($servicos as $key => $value){ ?>
                                    <tr>
                                        <td><?= ucwords(strtolower($value->descricao));     ?></td>
                                        <td><?= $value->tempoEstimado; ?></td>
                                        <td><?= "R$ ". number_format($value->preco, 2 , ',' , '')?></td>
                                        <td><a href="cadServico.php?update=<?= $value->idServico?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a></td>
                                        <td><button  class="btn btn-danger"
									     onclick="confirmar('<?= $value->idServico ?>' , 
										 '<?= ucwords(strtolower($value->descricao)) ?>')"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                  <?php } ?>
                                   
                                </tbody>
                            </table>
							</div>			       
                        
                </div>
			</div>
		</div>
		
       <script>
	   
	    function confirmar(idServico, nomeServico){

			swal({
				title: "Deseja realmente excluir o serviço " + nomeServico +" ?",
				text: "",
				icon: "warning",
				buttons: ['cancelar', 'ok'],
				dangerMode: true,
				})
				.then((willDelete) => {
				if (willDelete) {
					
					window.location.href = "?delete="+idServico;

				} else {
					
					return null;
				}
				});
         
		    return false;
			 

		}
	   
	   </script>
	

		<!-- end #content -->