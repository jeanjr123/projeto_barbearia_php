
		<!-- begin #content -->
		<div id="content" class="content">
		
			<!-- begin breadcrumb --->
			<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
			<!-- end page-header -->
			
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar" ></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			        </div>
					<?php      
					//chamando a funcao que foi carregada no loader
					renderTitle(
					 "Cadastro de Usuário"
					)				
					?>
			    </div>
				<div class="panel-body">
                <form action="cadUser.php" method="POST">
                                <fieldset>
                                <?php   include(TEMPLATE_PATH . '/messages.php');   ?>

                                         
                                
                                            <legend> <i class="fa fa-user"></i> Usuário  
                                            <button type="button" style="margin-left: 90%"  class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">
                                            <i data-toggle="tooltip" class="fa fa-camera fa-5"
                                            data-placement="top"   title="Adicionar Foto">
                                            </i> </button>

                                           


                                           </legend>
                                       
                                           
                             
                                 <?php if(isset($_GET['update'])){   ?>
                                    <input type="hidden" name="idUsuario" value="<?=  $_GET['update'] ?>">
                                 <?php } ?>           

                                    <div class="form-group col-md-6">
                                        <label for="nomeUsuario">Nome</label>
                                        <input type="text" class="form-control" id="nomeUsuario" name="nomeUsuario"
                                         placeholder="Insira o nome" <?= $errors['nomeUsuario'] ? 'is-invalid' : '' ?>
                                         value="<?= $nomeUsuario ?>" />
                                         <div class="invalid-feedback" style="color:red">
                                            <?= $errors['nomeUsuario'] ?>
                                         </div>
                                    </div>

                                 

                                    <div class="form-group col-md-6">
                                        <label for="login">Login</label>
                                        <input type="text" class="form-control" id="login" name="login"
                                         placeholder="Insira o login"  <?= $errors['login'] ? 'is-invalid' : '' ?>
                                         value="<?= $login ?>"/>
                                         <div class="invalid-feedback" style="color: red" >
                                            <?= $errors['login'] ?>
                                         </div>

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="cpf">CPF</label>
                                        <input type="text" class="form-control" id="cpf" name="cpf" 
                                        placeholder="Insira o cpf" <?= $errors['cpf'] ? 'is-invalid' : '' ?>
                                        value="<?= $cpf ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['cpf'] ?>
                                         </div>
                                    </div>


                                    <div class="form-group col-md-6">
                                        <label for="rg">RG</label>
                                        <input type="text" class="form-control" id="rg" name="rg" 
                                        placeholder="Insira o rg"  <?= $errors['rg'] ? 'is-invalid' : '' ?> 
                                        value="<?= $rg ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['rg'] ?>
                                         </div>
                                    </div>


                                    <div class="form-group col-md-6">
                                        <label for="celular">Celular</label>
                                        <input type="text" class="form-control" id="celular" name="celular" 
                                        placeholder="Insira o celular"  <?= $errors['celular'] ? 'is-invalid' : '' ?>
                                        value="<?= $celular ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['celular'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="telefoneFixo">Telefone fixo</label>
                                        <input type="text" class="form-control" id="telefoneFixo" name="telefoneFixo"
                                         placeholder="Insira o telefone fixo"  <?= $errors['telefoneFixo'] ? 'is-invalid' : '' ?> 
                                         value="<?= $telefoneFixo ?>"/>
                                         <div class="invalid-feedback" style="color: red">
                                            <?= $errors['telefoneFixo'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" 
                                        placeholder="Insira o e-mail"  <?= $errors['email'] ? 'is-invalid' : '' ?>
                                        value="<?= $email ?>" />
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['email'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="senha">Senha</label>
                                        <input type="password" class="form-control" id="senha" name="senha" 
                                        placeholder="Insira a senha"  <?= $errors['senha'] ? 'is-invalid' : '' ?> 
                                        value="<?= $senha ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['senha'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="confirme_senha">Confirmar Senha</label>
                                        <input type="password" class="form-control" id="confirme_senha" name="confirme_senha" 
                                        placeholder="Confirme a senha"  <?= $errors['confirme_senha'] ? 'is-invalid' : '' ?>
                                        value="<?= $confirme_senha  ?>" />
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['confirme_senha'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="dataNasc">Data Nascimento</label>
                                        <input type="date" class="form-control" id="dataNasc" name="dataNasc" 
                                        placeholder="Insira o bairro" <?= $errors['dataNasc'] ? 'is-invalid' : '' ?>
                                        value="<?= $dataNasc ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['dataNasc'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="cep">CEP</label>
                                        <input type="text" class="form-control cep" id="cep" name="cep" 
                                        placeholder="Insira o cep" <?= $errors['cep'] ? 'is-invalid' : '' ?>
                                        value="<?= $cep ?>" />
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['cep'] ?>
                                         </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="enderecoLogradouro">Logradouro</label>
                                        <input type="text" class="form-control" id="enderecoLogradouro" name="enderecoLogradouro"
                                         placeholder="Insira o logradouro ex: Rua , Avenida.." 
                                          <?= $errors['enderecoLogradouro'] ? 'is-invalid' : '' ?>
                                          value="<?= $enderecoLogradouro ?>"/>
                                          <div class="invalid-feedback" style="color: red">
                                            <?= $errors['enderecoLogradouro'] ?>
                                         </div>

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="numero">Numero</label>
                                        <input type="text" class="form-control" id="numero" name="numero" 
                                        placeholder="Insira o numero" <?= $errors['numero'] ? 'is-invalid' : '' ?>
                                        value="<?= $numero ?>"/>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['numero'] ?>
                                         </div>
                                    </div>


                                    <div class="form-group col-md-6">
                                        <label for="bairro">Bairro</label>
                                        <input type="text" class="form-control" id="bairro" name="bairro"
                                         placeholder="Insira o bairro" <?= $errors['bairro'] ? 'is-invalid' : '' ?>
                                         value="<?= $bairro ?>" />
                                         <div class="invalid-feedback" style="color: red">
                                            <?= $errors['bairro'] ?>
                                         </div>
                                    </div>

                               <!--  TEM QUE USAR API CORREIOS -->     


                                    <div class="form-group col-md-6">
                                    <label for="estado">Cidade</label>

                                   
                                        <select class="form-control" id="cidade" name="idCidade" 
                                         <?= $errors['idCidade'] ? 'is-invalid' : '' ?>>
                                         <option value="">Selecione a cidade</option>
                                            <?php   foreach($cidades as $key => $value){   ?>       
                                            <option value="<?= $value->idCidade ?>"><?= ucwords(strtolower($value->nomeCidade)) . " / " . strtoupper($value->sigla)?></option>
                                            <?php  } ?>
                                        </select>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['idCidade'] ?>
                                         </div>
                                    </div>
                                    

                                    <div class="form-group col-md-6">
                                    <label for="estado">Status</label>

                                   
                                        <select class="form-control" id="status" name="status">
                                            <option value="ATIVO">Ativo</option>
                                            <option value="INATIVO">Inativo</option>
                                        </select>
                                      
                                    </div> 




                                    <div class="form-group col-md-6">
                                    <label for="estado">Tipo</label>
                                        <select class="form-control" id="tipo" name="idTipo"
                                        <?= $errors['idTipo'] ? 'is-invalid' : '' ?>>
                                         <option value="">Selecione o tipo</option>
                                            <?php   foreach($tipos as $key => $value){   ?>       
                                            <option value="<?= $value->idTipo ?>"><?= ucwords(strtolower($value->descricaoTipo))?></option>
                                            <?php  } ?>
                                        </select>
                                        <div class="invalid-feedback" style="color: red">
                                            <?= $errors['idTipo'] ?>
                                         </div>
                                    </div>  

                                   
 



                                </fieldset>
                                       <div class="col-md-10">
                                            <button type="submit" class="btn btn-sm btn-success">Cadastrar</button>
                                        </div>
                            </form>
				       
                </div>
			</div>
        </div>
        


 

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document" style="width: 35%">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel"><strong><i class="fa fa-user"></i> Foto Usuario</strong></h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row">
         <div style="margin-left: 22%">
           <video id="player"  width=300 height=250 controls autoplay style="display:block"></video>
         <div>
         <div>
           <canvas id="snapshot" width=300 height=250 style="display:none"></canvas>
         <div>
        </div>
      </div>
      <div class="modal-footer">
 
     
      <button id="capture" class="btn btn-info">Capturar</button>
      <button id="cancel" class="btn btn-warning">Cancelar</button>
        <button id="salvar" type="button" class="btn btn-primary">Salvar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<script>
 
 $(document).on("click","#capture", function(){

    $("#player").attr("style", "display:none");
   
    $("#snapshot").attr("style", "display:block");

 });


  
 $(document).on("click","#cancel", function(){

        $("#player").attr("style", "display:block");

        $("#snapshot").attr("style", "display:none");

});




</script>



<script>
  var player = document.getElementById('player'); 
  var snapshotCanvas = document.getElementById('snapshot');
  var captureButton = document.getElementById('capture');

   

  var handleSuccess = function(stream) {
    // Attach the video stream to the video element and autoplay.
    player.srcObject = stream;

   
  };

  captureButton.addEventListener('click', function() {
    var context = snapshot.getContext('2d');
    // Draw the video frame to the canvas.
    context.drawImage(player, 0, 0, snapshotCanvas.width, 
        snapshotCanvas.height);

    
       
  });

  navigator.mediaDevices.getUserMedia({video: true})
      .then(handleSuccess);

       



    
</script>


    <script type="text/javascript">
        $("#celular").mask("(00) 00000-0000");
        $("#telefoneFixo").mask("(00) 0000-0000");
        $("#cpf").mask("000.000.000-00");
        $("#rg").mask("00.000.000-0");
        $("#cep").mask("00.000-000");



         $(document).on("blur",".cep", function(){

            var cep = $("#cep").val();

            $.ajax({
               url: "/apiCorreios.php", 
              type: "POST" ,
              datatype: "json",
              data:"cep="+ cep,
              success: function(data){
                  var jsonData = JSON.parse(data);
                   $('#enderecoLogradouro').val(jsonData.logradouro);

                   $('#bairro').val(jsonData.bairro);
               }
               
            });


         })
    </script>

  

    
        
    
        <!-- end #content -->
        
