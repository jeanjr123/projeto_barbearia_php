	
		<!-- begin #content -->
		<div id="content" class="content">

		
			<!-- begin breadcrumb --->
			<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
			<!-- end page-header -->
			
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar" ></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			        </div>
					<?php      
					//chamando a funcao que foi carregada no loader
					renderTitle(
					 "Barbearias Cadastradas"
					)				
					?>
			    </div>
				<div class="panel-body">
                
                  <a class="btn btn-primary" style="margin-bottom:15px" href="cadBarbearia.php">
                  <i class="fa fa-plus"></i> Nova Barbearia</a>
                  <?php   include(TEMPLATE_PATH . '/messages.php');   ?>



                  <div class="table-responsive">
                  <table id="data-table" class="table table-striped table-bordered nowrap table-hover" width="100%">
                                <thead>
                                <tr>                                  
                                <th>Razao Social</th>
                                <th>Nome Fantasia</th>
                                <th>CNPJ</th> 
                                <th>Inscricao Estadual</th>
                                <th>Telefone 1</th>
                                <th>Telefone 2</th> 
                                <th>Logradouro</th> 
                                <th>Nro</th> 
                                <th>Bairro</th>
                                <th>Cep</th>
                                <th>Email</th>
                                <th>Cidade</th>  
                                <th>Estado</th>  
                                <th>Editar</th>
                                <th>Excluir</th> 
                                </tr>
                                </thead>
                                <tbody>
                                 <?php foreach($barbearias as $key => $value){ ?>
                                    <tr>                                 
                                    <td><?= ucwords(strtolower($value->razaoSocial))        ?></td>
                                    <td><?= ucwords(strtolower($value->nomeFantasia))       ?></td>
                                    <td><?= $value->cnpj                                    ?></td> 
                                    <td><?= $value->inscricaoEstadual                       ?></td>
                                    <td><?= $value->telefone1                               ?></td>
                                    <td><?= $value->telefone2                               ?></td> 
                                    <td><?= ucwords(strtolower($value->enderecoLogradouro)) ?></td> 
                                    <td><?= $value->numero                                  ?></td> 
                                    <td><?= ucwords(strtolower($value->bairro))             ?></td>
                                    <td><?= $value->cep                                     ?></td>
                                    <td><?= strtolower($value->email)                       ?></td>
                                    <td><?= ucwords(strtolower($value->nomeCidade))        ?></td>    
                                    <td><?= ucwords(strtolower($value->nomeEstado))        ?></td>   
                                    <td><a href="cadBarbearia.php?update=<?= $value->idSalao   ?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a></td>  
                                    <td><button class="btn btn-danger" onclick="confirmar('<?= $value->idSalao ?>' , 
										 '<?= ucwords(strtolower($value->nomeFantasia)) ?>')"><i class="fa fa-trash"></i></button></td>  
                                    </tr>  
                                 <?php  }   ?>              
                                </tbody>
                            </table>
							</div>                 
                </div>
			</div>
		</div>
    
		<!-- end #content -->


		<script>
	   
	   function confirmar(idBarbearia, nomeBarbearia){

		   swal({
			   title: "Deseja realmente excluir o estabelecimento " + nomeBarbearia +" ?",
			   text: "",
			   icon: "warning",
			   buttons: ['cancelar', 'ok'],
			   dangerMode: true,
			   })
			   .then((willDelete) => {
			   if (willDelete) {
				   
				   window.location.href = "?delete="+idBarbearia;

			   } else {
				   
				   return null;
			   }
			   });
		
		   return false;
			

	   }
	  
	  </script>

	