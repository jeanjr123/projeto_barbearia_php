	
		<!-- begin #content -->
		<div id="content" class="content">
        

		
			<!-- begin breadcrumb --->
			<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
			<!-- end page-header -->
			
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar" ></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			        </div>
					<?php      
					//chamando a funcao que foi carregada no loader
					renderTitle(
					 "Cadastro de Estado"
					)				
					?>
			    </div>
				<div class="panel-body">
                <form action="cadEstado.php" method="POST">
                                <fieldset>
                                <?php   include(TEMPLATE_PATH . '/messages.php');   ?>
             
                                    <legend><i class="fa fa-map"></i> Estado </legend>

                                    <?php if(isset($_GET['update'])){ ?>
                                      <input type="hidden" name="idEstado" value="<?= $_GET['update'] ?>">
									<?php  }   ?>

                                    <div class="form-group col-md-6">
                                        <label for="nomeEstado">Nome Estado</label>
                                        <input type="text" class="form-control" id="nomeEstado" name="nomeEstado"
										 placeholder="Insira o nome do estado" 
										 <?= $errors['nomeEstado'] ? 'is_invalid' : ''  ?>
										 value="<?= $nomeEstado ?>"/>
										 <div class="class-feedback" style="color:red">
										   <?= $errors['nomeEstado'] ?>
										 </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="siglaEstado">Sigla Estado</label>
                                        <input type="text" class="form-control" id="siglaEstado" name="sigla"
										 placeholder="Insira a sigla do estado" 
										 <?= $errors['sigla'] ? 'is_invalid' : '' ?>
										 value="<?= $sigla ?>"/>
										 <div class="class-feedback" style="color:red">
										   <?= $errors['sigla'] ?>
										 </div>
                                   
								    </div>

                                           
                                       
                                                                    
                                </fieldset>
                                       <div class="col-md-10">
                                            <button type="submit" class="btn btn-sm btn-success">Cadastrar</button>
                                        </div>
                            </form>
				       
                </div>
			</div>
        </div>
        
    
        <!-- end #content -->
        
