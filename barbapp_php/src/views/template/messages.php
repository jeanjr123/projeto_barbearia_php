<?php

//sempre que carregar vem com errors vazio por default
$errors = [];


if(isset($_SESSION['message'])){

   $message = $_SESSION['message'];
   unset($_SESSION['message']);


   
}else if(isset($exception)){
   //entra somente se exception tiver setado no controller
  $message = [
     'type' => 'error',
     'message' => $exception->getMessage()
  ];

   if(get_class($exception) === 'ValidationException'){
     //se a classe que passou os erros para exception da pagina controller login
     //foi validation entao os erros desse exception podem passar para errors
     //pois vieram da modal Login que foram checadas pelo check login 
     //pelo controller login
        $errors = $exception->getErrors();
   }

}

$alertType = "";

if(isset($message)){
   if($message['type'] === 'error'){
      $alertType = 'danger';

   }else{
      $alertType = 'success';

   }

}







?>

<?php  if(isset($message)){   ?>
<div class="my-3 alert alert-<?= $alertType ?>" id="alerta" >
  <?= $message['message']; ?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button>
</div>

<?php
   

?>
       

<?php } ?>



