	<!-- begin #sidebar -->
    <div id="sidebar" class="sidebar sidebar-transparent">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<div class="image">
							<a href="javascript:;"><img src="assets/img/user-13.jpg" alt="" /></a>
						</div>
						<div class="info">
							<?= ucwords(strtolower($_SESSION['user']->nomeUsuario)) ?>
							<small>Tipo Usuario</small>
						</div>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header"><strong style="color: white;">Menu</strong></li>
					<li class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-calendar"></i>
						    <span>Agenda</span>
					    </a>
						<ul class="sub-menu">
						    <li><a href="agendamento.php">Agendar Horário </a></li>
						    <li><a href="agendamento.php?compromissos=1" >Listar Horários</a></li>
						</ul>
					</li>
				
					<li class="has-sub">
					    <a href="javascript:;">
					        <b class="caret pull-right"></b>
					        <i class="fa fa-user"></i>
					        <span>Usuário</span>
					    </a>
					    <ul class="sub-menu">
						    <?php if($_SESSION['user']->idTipo == 2){ ?>
							<li><a href="cadUser.php">Cadastrar Usuário </a></li>
							<?php } ?>
						    <li><a href="user.php">Listar Usuários</a></li>
					    </ul>
					</li>


					<?php if($_SESSION['user']->idTipo == 2){ ?>
					<li class="has-sub">
					    <a href="javascript:;">
					        <b class="caret pull-right"></b>
					        <i class="fa fa-cut"></i>
					        <span>Serviços</span>
					    </a>
					    <ul class="sub-menu">
						    <li><a href="cadServico.php">Cadastrar Serviço </a></li>
						    <li><a href="servico.php">Listar Serviços</a></li>
					    </ul>
					</li>
					<?php } ?>


					<?php if($_SESSION['user']->idTipo == 2){ ?>
					<li class="has-sub">
					    <a href="javascript:;">
					        <b class="caret pull-right"></b>
					        <i class="fa fa-building"></i>
					        <span>Barbearia</span>
					    </a>
					    <ul class="sub-menu">
						    <li><a href="cadBarbearia.php">Cadastrar Barbearia</a></li>
						    <li><a href="barbearia.php">Listar Barbearias</a></li>
					    </ul>
					</li>
					<?php } ?>

					<?php if($_SESSION['user']->idTipo == 2){ ?>
					<li class="has-sub">
					    <a href="javascript:;">
					        <b class="caret pull-right"></b>
					        <i class="fa fa-cogs"></i>
					        <span>Configurações </span>
					    </a>
					    <ul class="sub-menu">
						    <li><a href="cadCidade.php">Cadastrar Cidade</a></li>
							<li><a href="cidade.php">Listar Cidades</a></li>
							<li><a href="cadEstado.php">Cadastrar Estado</a></li>
						    <li><a href="estado.php">Listar Estados</a></li>
					    </ul>
					</li>
					<?php } ?>


			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
		