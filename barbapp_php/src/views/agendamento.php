
<script>



var h = {
		   left: 'prev,next today',
		   center: 'title',
		   right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'

}

if(window.innerWidth < 600){

  

   h = {
		   left: 'prev,next today',
		   center: '',
		   right: 'dayGridMonth,timeGridWeek,timeGridDay'

   }



}









</script>












<script>
	document.addEventListener('DOMContentLoaded', function() {
		var calendarEl = document.getElementById('calendar');

		var calendar = new FullCalendar.Calendar(calendarEl, {
		

			locale: 'pt-br',
			plugins: ['dayGrid', 'timeGrid', 'list', 'interaction'],
			header: h,
			//defaultDate: '2019-08-12', comment for get current date
			navLinks: true, // can click day/week names to navigate views
			selectable: true,
			selectMirror: true,
			editable: true,
			minTime: "08:00",
            maxTime: "20:00",
			eventLimit: true, // allow "more" link when too many events

			events: [
				<?php
					
					foreach ($agendamentos as $key => $value) { ?> 
				    {

						title: '<?= ($_SESSION["user"]->idUsuario == $value->idCliente || $_SESSION["user"]->idTipo == 2) ? ucwords(strtolower($value->nomeCliente)) : "Reservado" ?>',
						title2: 'haha',
						//url: '/user.php',
						start: '<?= $value->horaInicio    ?>',
						end: '<?= $value->horaFim       ?>',
						backgroundColor:  '<?= ($_SESSION["user"]->idUsuario == $value->idCliente || $_SESSION["user"]->idTipo == 2) ? "#4682B4" : "#ff5b57" ?>',
						extendedProps: {
							
							nomeCliente: '<?= ($_SESSION["user"]->idUsuario == $value->idCliente || $_SESSION["user"]->idTipo == 2 ) ? ucwords(strtolower($value->nomeCliente)) : ""  ?>',
							nomeBarbeiro: '<?= ucwords(strtolower($value->nomeCabelereiro))  ?>',
							horaInicio: '<?= substr($value->horaInicio, 11, 8) ?>',
							horaTermino: '<?= substr($value->horaFim, 11, 8) ?>',
							servicos: '<?= ucwords(strtolower($value->servicosCliente))  ?>',
							totalPagar: '<?= "R$ " . number_format($value->preco, 2, ",", ".") ?>',
							idAgendamento: '<?= $value->idAgendamento ?>',
							escondeExcluir: '<?= ($_SESSION["user"]->idUsuario == $value->idCliente) ? "" : "none" ?>',


						}

					},


				<?php } ?>

			],
            
			eventClick: function(info) {
				$('#modal-dialog #nomeCliente').text(info.event.extendedProps.nomeCliente);
				$('#modal-dialog #nomeBarbeiro').text(info.event.extendedProps.nomeBarbeiro);
				$('#modal-dialog #horaInicio').text(info.event.extendedProps.horaInicio);
				$('#modal-dialog #horaTermino').text(info.event.extendedProps.horaTermino);
				$('#modal-dialog #servicos').text(info.event.extendedProps.servicos);
				$('#modal-dialog #totalPagar').text(info.event.extendedProps.totalPagar);
				$('#modal-dialog #excluir').prop("href", "?delete="+info.event.extendedProps.idAgendamento);
				$('#modal-dialog #excluir').prop("style", "display:"+info.event.extendedProps.escondeExcluir);
				$('#modal-dialog').modal('show');
			},

			select: function(info) {
				// alert('Inicio do evento  ' + info.start.toLocaleString() ); 
				$('#modal-agendamento #data').val(info.start.toLocaleString().substr(0, 10));
				$('#modal-agendamento #horaInicio').val(info.start.toLocaleString().substr(11,8));
				$('#modal-agendamento #horaFim').val(info.start.toLocaleString().substr(11,8));
				$('#modal-agendamento').modal('show');

			}

		});

		calendar.render();
	});
</script>



<style>
	body {
		margin: 40px 10px;
		padding: 0;
		font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}
</style>

<link href="assets/css/multi-select.css" rel="stylesheet" />




<div id="content" class="content">



	<!-- begin breadcrumb --->
	<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			</div>
			<?php
			//chamando a funcao que foi carregada no loader
			renderTitle(
				"Cadastro de Agendamento"
			);

			?>
		</div>
		<div class="panel-body">


			<legend> <i class="fa fa-calendar"></i> Agendamento </legend>



			<div id='calendar'></div>

		</div>
	</div>
</div>


<div class="modal fade" id="modal-dialog">
	<div class="modal-dialog">
		<div class="modal-content" style="background-color: #242a30">
			<div class="modal-header">
				<button type="button" class="close" style="color:white !important;" data-dismiss="modal">×</button>
				<h4 class="modal-title" style="color:white"><i class="fa fa-id-card"></i> Detalhes do Agendamento</h4>
			</div>
			<div class="modal-body bg-white">
				<a class="media-left" href="javascript:;">
					<img src="assets/img/user-5.jpg" alt="" class="media-object rounded-corner" />
				</a>
				<div class="media-body">
					<style>
						h5 {
							margin-right: 15px;
						}
					</style>
					<h5 class="media-heading"><strong>Cliente:</strong>&nbsp;<span id="nomeCliente"></span></h5>
					<h5><strong>Barbeiro:</strong>&nbsp;<span id="nomeBarbeiro"></span></h5>
					<h5><strong>Horário Inicio:</strong>&nbsp;<span id="horaInicio"></span></h5>
					<h5><strong>Horário Término:</strong>&nbsp;<span id="horaTermino"></span></h5>
					<h5><strong>Serviços:</strong>&nbsp;<span id="servicos"></span></h5>
					<h5><strong>Total a pagar:</strong>&nbsp;<span id="totalPagar"></span></h5>


				</div>


			</div>
			<div class="modal-footer bg-white">
				<a href="javascript:;" class="btn btn-sm btn-success" data-dismiss="modal"><strong>Fechar</strong></a>
			   
				
					<a href="" id="excluir" class="btn btn-sm btn-danger"><strong>Excluir</strong></a>
				

			</div>
		</div>
	</div>
</div>




<div class="modal fade" id="modal-agendamento">
	<div class="modal-dialog">
		<div class="modal-content" style="background-color: #242a30">
			<div class="modal-header">
				<button type="button" class="close" style="color:white !important;" data-dismiss="modal">×</button>
				<h4 class="modal-title" style="color:white"><i class="fa fa-calendar"></i> Agendamento</h4>
			</div>
			<div class="modal-body bg-white">

				<form action="agendamento.php" method="POST">
					<fieldset>
						 <?php //include(TEMPLATE_PATH . '/messages.php');   ?> 



						<?php if (isset($_GET['update'])) { ?>
							<input type="hidden" name="idSalao" value="<?= $_GET['update'] ?>">
						<?php } ?>

						<div class="form-group col-md-6">
							<label for="razaoSocial">Cliente</label>
							<input style ="color: black" type="text" class="form-control" id="idUsuario" name="idUsuario" <?= $erros['idUsuario'] ? 'is_invalid' : '' ?> value="<?= ucwords(strtolower($_SESSION['user']->nomeUsuario)) ?>" disabled />
						</div>



						<div class="form-group col-md-6">
							<label for="usuario">Barbeiro</label>
							<select class="form-control" id="usuario" name="idCabelereiro"  
							required oninvalid="this.setCustomValidity('Selecione um barbeiro!')"
							onchange="try{setCustomValidity('')}catch(e){}">
								<option value="">Selecione um barbeiro</option>
								<?php foreach ($barbeiros as $key => $value) { ?>
									<option value="<?= $value->idUsuario ?>"><?= ucwords(strtolower($value->nomeUsuario)) ?></option>
								<?php  } ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label for="salao">Barbearias</label>
							<select class="form-control" id="salao" name="idSalao"  
							required oninvalid="this.setCustomValidity('Selecione uma barbearia!')"
							onchange="try{setCustomValidity('')}catch(e){}"
							>
								<option value="">Selecione uma barbearia</option>
								<?php foreach ($barbearias as $key => $value) { ?>
									<option value="<?= $value->idSalao ?>"><?= ucwords(strtolower($value->nomeFantasia)) ?></option>
								<?php  } ?>
							</select>
						</div>


						<div class="form-group col-md-6">
							<label for="data">Data</label>
							<input type="text" class="form-control" id="data" name="data" placeholder="Insira a data" <?= $erros['data'] ? 'is_invalid' : '' ?> value="<?= $data   ?>" required/>
						</div>



						<div class="form-group col-md-12 ofset-2">
							<label for="data">Selecione os servicos</label>
							<select id='callbacks' multiple='multiple' 
							required oninvalid="this.setCustomValidity('Selecione pelo menos um servico!')"
							onchange="try{setCustomValidity('')}catch(e){}">
								<?php foreach ($servicos as $key => $value) { ?>
									<option id="<?= $value->idServico ?>" value="id <?= $value->idServico ?>,preco <?= $value->descricao ?> <?= $value->preco ?>,tempo <?= $value->descricao ?> <?= $value->tempoEstimado ?>"><?= ucwords(strtolower($value->descricao))
																										. " /" . " R$ " . number_format($value->preco, 2, ',', '.') ?></option>
								<?php   } ?>
							</select>
						</div>


						<div class="form-group col-md-12">
							<label for="preco_total">Valor Total Serviços</label>
							<input type="text" class="form-control" id="preco_total" name="preco_total" 
							 <?= $erros['preco_total'] ? 'is_invalid' : '' ?> value="<?= $preco_total  ?>" disabled 
							 style="color:#4682B4; font-size: 20px"/>
						</div>


						<div class="form-group col-md-6">
							<label for="horaInicio">Hora Inicio</label>
							<input type="text" class="form-control" id="horaInicio" name="horaInicio" placeholder="Insira a hora inicio" <?= $erros['horaInicio'] ? 'is_invalid' : '' ?> value="<?= $horaInicio  ?>" required />
						</div>


						<div class="form-group col-md-6">
							<label for="horaFim">Hora Fim</label>
							<input type="text" class="form-control" id="horaFim" name="horaFim" placeholder="Insira a hora fim" <?= $erros['horaFim'] ? 'is_invalid' : '' ?> value="<?= $horaFim  ?>" required />
						</div>
					  
						<div class="form-group col-md-6">
							<input type="hidden"  id="services" value="" name="idServicos">
						</div>

					</fieldset>
			</div>
			<div class="modal-footer bg-white">
				<div class="col-md-12">
					<button type="submit" id="agendar" class="btn btn-sm btn-success">Cadastrar</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<script src="assets/plugins/jquery/jquery.multi-select.js"></script>


<!--  CARREGAR DIA AO CARREGAR PAGINA -->


<body onLoad="clickDay()">

<script>

function clickDay(){
	$(".fc-timeGridDay-button").click();
}

</script>

<script>
	let array = [];

	let arraySub = [];

	$('#callbacks').multiSelect({
		afterSelect: function(values) {

			array.push(values);


			//alert(array);

		    let hora_inicio = $('#modal-agendamento #horaInicio').val();


			

			$.ajax({

				url: 'agendamentoAjax.php',

				type: 'POST',

				dataType: 'json',
                
				data: 'array=' + array + '&arraySub=' + arraySub + '&valores=' + values + '&hora_inicio=' + hora_inicio,

				success: function(data){
					
                  $('#preco_total').val(data.preco_total);

				  $('#horaFim').val(data.hora_final);

				  $('#services').val(data.total_id);

				
				  
				}
			});

			//alert("Servico escolhido: " + values);
		},
		afterDeselect: function(values) {

           arraySub.push(values);
		   $.ajax({

				url: 'agendamentoAjax.php',

				type: 'POST',

				dataType: 'json',

				data: 'array=' + array + '&arraySub=' + arraySub + '&valores=' + values,

				success: function(data){
					
				  $('#preco_total').val(data.preco_total);

				 

                  arraySub = [];
 				  
				}
			});

			alert("Deselect value: " + values);
		}

      
	});

</script>


   
  



                         <!-- <script>
								$(document).ready(function() {
									App.init();
									TableManageButtons.init();
									FormPlugins.init();

								});
                           </script> -->