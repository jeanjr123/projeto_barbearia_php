	
		<!-- begin #content -->
		<div id="content" class="content">
        

		
			<!-- begin breadcrumb --->
			<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
			<!-- end page-header -->
			
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar" ></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			        </div>
					<?php      
					//chamando a funcao que foi carregada no loader
					renderTitle(
					 "Cadastro de Serviço"
					)	;	
					 
					?>
			    </div>
				<div class="panel-body">
                <form action="cadServico.php" method="POST">
                                <fieldset>
                                <?php   include(TEMPLATE_PATH . '/messages.php');   ?>
								<?php if(isset($_GET['update'])){ ?>
								  <input type="hidden" name="idServico" value="<?= $idServico ?>">
								<?php } ?> 
                                    <legend><i class="fa fa-cut"></i> Serviço </legend>
                                    <div class="form-group col-md-6">
                                        <label for="descricaoServico">Descrição Serviço</label>
                                        <input type="text" class="form-control" id="descricaoServico" name="descricao" 
										placeholder="Insira a descricao do serviço"
										<?= $errors['descricao'] ? "is_invalid" : "" ?> 
										value = "<?= $descricao ?>"/>
										<div class="invalid-feedback" style="color: red">
										  <?= $errors['descricao']  ?>
										</div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="tempoEstimado">Tempo Estimado Min</label>
                                        <input type="time" step="5" class="form-control" id="tempoEstimado" name="tempoEstimado"
										 <?= $errors['tempoEstimado'] ? "is_invalid" : "" ?> 
										value = "<?= $tempoEstimado ?>"/>
										<div class="invalid-feedback" style="color: red">
										  <?= $errors['tempoEstimado']  ?>
										</div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="precoServico">Preço Serviço </label>
                                        <input type="text" class="form-control" id="precoServico" name="preco" 
										placeholder="Insira o preço do serviço"
										<?= $errors['preco'] ? "is_invalid" : "" ?> 
										value = "<?= $preco ?>" />
										<div class="invalid-feedback" style="color: red">
										  <?= $errors['preco']  ?>
										</div>
                                    </div>
                                                         
                                       
                                                                    
                                </fieldset>
                                       <div class="col-md-10">
                                            <button type="submit" class="btn btn-sm btn-success" 
											id="cadastrar">Cadastrar</button>
                                        </div>
							</form>
							
							
				       
                </div>
			</div>
        </div>
        <script type="text/javascript">
        $("#precoServico").mask("00,00");

		</script>

		
     
        <!-- end #content -->
        
