	
		<!-- begin #content -->
		<div id="content" class="content">

		
			<!-- begin breadcrumb --->
			<!-- <ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Page Options</a></li>
				<li class="active">Page with Mega Menu</li>
			</ol> -->
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<!-- <h1 class="page-header">Page with Mega Menu <small>header small text goes here...</small></h1> -->
			<!-- end page-header -->
			
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat centralizar"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus centralizar" ></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times centralizar"></i></a>
			        </div>
					<?php      
					//chamando a funcao que foi carregada no loader
					renderTitle(
					 "Home"
					)			
					
		
					?>
					
			    </div>
				<div class="panel-body">

				<h5>Dashboards </h5>
						   <hr>

				<div class="row">
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-green">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-clock-o fa-fw"></i></div>
			            <div class="stats-title"><strong>AGENDAMENTOS DO DIA</strong></div>
			            <div class="stats-number"><?= $totalAgendamentos ?></div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 70.1%;"></div>
                        </div>
                        <div class="stats-desc"><strong></strong></div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			    <!-- <div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-blue">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-clock-o fa-fw"></i></div>
			            <div class="stats-title"><strong>AGENDAMENTOS DA SEMANA</strong></div>
			            <div class="stats-number">18</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 40.5%;"></div>
                        </div>
                        <div class="stats-desc"><strong>Melhor que ultima semana (40.5%)</strong></div>
			        </div>
			    </div>
			  
			   
			    <div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-black">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-user fa-fw"></i></div>
			            <div class="stats-title"><strong>CLIENTES FIDELIDADE</strong></div>
			            <div class="stats-number">2</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 54.9%;"></div>
                        </div>
                        <div class="stats-desc"><strong>Agendados 0</strong></div>
			        </div> 
			    </div> -->
			</div>
					
							
             </div>
			</div>
			
		</div>

		
    
		<!-- end #content -->